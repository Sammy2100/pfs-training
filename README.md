# PFS Training

This is the repository that holds all PFS training douments, slides and projects - Frontend React and Backend ASP.NET Core WebApi v5

### What is this repository for?

- This is the repository where all training participants push their coding practice and take home assignments

### How do I get set up?

- Clone the repo
- Create a branch having yourname-yoursurname
- Commit Push the created branch whenever you update your source code

### Tools reqired

- Visual Studio
- Visual Studio Code
- Node.JS

### Who do I talk to?

- Samuel Iyomere
- Hameedah Adebanjo
