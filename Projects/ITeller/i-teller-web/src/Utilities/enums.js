export const OPERATORTYPE = {
    Administrator: 1,
    Operator: 2
}

export const CHEQUESTATUS = {
    Pending: 1,
    Rejected: 2,
    Approved: 3
}