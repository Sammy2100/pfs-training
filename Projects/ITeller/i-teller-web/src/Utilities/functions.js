import Jwt from "jsonwebtoken";
import { JwtConfig } from "../config.json";

import { USERKEY } from "./constants";
import { CHEQUESTATUS } from "./enums";

export function parseChequeStatus(status) {
  switch (status) {
    case CHEQUESTATUS.Pending:
      return "Pending";
    case CHEQUESTATUS.Rejected:
      return "Rejected";
    case CHEQUESTATUS.Approved:
      return "Approved";
    default:
      break;
  }
}

export function getUserToken() {
  return localStorage.getItem(USERKEY);
}

export function getUserFromLocalStorage() {
  const token = getUserToken();
  const secret = JwtConfig.secret;
  let user = Jwt.decode(token, secret);
  return user;
}

export function logout() {
  localStorage.removeItem(USERKEY);
  localStorage.clear();
  window.location = "/";
}

export function formatDate(date) {
  if (!date) return "";

  const newDate = new Date(date);
  const year = newDate.getFullYear();
  let month = newDate.getMonth() + 1;

  const formattedDate = `${year}-${month.toString().padStart(2, "0")}-${newDate
    .getDate()
    .toString()
    .padStart(2, "0")}`;

  return formattedDate;
}

export function serviceError(err, setErrorMessage, setShowError) {
  if (err.response && err.response.data) {
    setErrorMessage(err.response.data);
    setShowError(true);
  } else {
    setErrorMessage("An exception occured!");
    setShowError(true);
  }
}

export function monetizeAmount(amount) {
  if (!amount || amount === "") amount = 0;
  let numericAmount = Number(amount);
  return "" + Number(numericAmount.toFixed(2)).toLocaleString();
}
