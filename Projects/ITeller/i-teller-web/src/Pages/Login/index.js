import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { Formik } from "formik";
import * as Yup from "yup";
import { USERKEY } from "../../Utilities/constants";
import { loginUser } from "../../Services/userServices";

const ColorButton = withStyles(() => ({
  root: {
    color: "#fff",
    backgroundColor: "#4B0082",
    "&:hover": {
      backgroundColor: "#8A2BE2",
    },
  },
}))(Button);

const Login = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [showError, setShowError] = useState(false);
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <Formik
      initialValues={{
        emailAddress: "",
        password: "",
      }}
      validationSchema={Yup.object().shape({
        emailAddress: Yup.string()
          .email("Invalid Email Format")
          .required("Please provide your email address"),
        password: Yup.string().required("Please provide your password"),
      })}
      onSubmit={async (values, { setSubmitting, resetForm }) => {
        try {
          let res = await loginUser(values);
          localStorage.setItem(USERKEY, res.data);
          window.location = "/dashboard";
          resetForm({ values: "" });
        } catch (err) {
          if (err.response && err.response.data) {
            setShowError(true);
            setErrorMessage(err.response.data);
          } else {
            setShowError(true);
            setErrorMessage("An exception occured");
          }
        }
        setSubmitting(false);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => {
        return (
          <div className="home">
            <div>
              <div className="loginFormHeader">
                <span className="iTellerText">I -Teller</span>
                <span className="loginText">Login</span>
              </div>
              <div className="loginform">
                <form style={{ width: "80%" }}>
                  <FormControl fullWidth>
                    <TextField
                      error={errors.emailAddress && touched.emailAddress}
                      id="emailAddress"
                      label="Email Address"
                      name="emailAddress"
                      value={values.emailAddress}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      style={{ margin: "20px 8px" }}
                      placeholder="Email Address"
                      helperText={
                        errors.emailAddress &&
                        touched.emailAddress &&
                        errors.emailAddress
                      }
                      fullWidth
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="outlined"
                    />
                  </FormControl>
                  <FormControl fullWidth>
                    <TextField
                      error={errors.password && touched.password}
                      id="Password"
                      label="Password"
                      type={showPassword ? "text" : "password"}
                      name="password"
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      style={{ margin: "20px 8px" }}
                      placeholder="Placeholder"
                      helperText={
                        errors.password && touched.password && errors.password
                      }
                      fullWidth
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={handleClickShowPassword}
                              onMouseDown={handleMouseDownPassword}
                              edge="end"
                            >
                              {showPassword ? (
                                <Visibility />
                              ) : (
                                <VisibilityOff />
                              )}
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                      variant="outlined"
                    />
                  </FormControl>
                  <div className="loginButton">
                    <ColorButton
                      variant="contained"
                      color="primary"
                      type="submit"
                      disabled={isSubmitting}
                      onClick={handleSubmit}
                    >
                      {isSubmitting ? "Logging In..." : "Login"}
                    </ColorButton>
                  </div>
                  {showError && (
                    <div>
                      <p style={{ color: "red" }}>{errorMessage}</p>
                    </div>
                  )}
                </form>
              </div>
            </div>
          </div>
        );
      }}
    </Formik>
  );
};

export default Login;
