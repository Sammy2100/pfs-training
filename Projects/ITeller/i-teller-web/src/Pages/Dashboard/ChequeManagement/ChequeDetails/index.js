import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  monetizeAmount,
  parseChequeStatus,
} from "../../../../Utilities/functions";
import { Grid } from "@material-ui/core";
import { CHEQUESTATUS } from "../../../../Utilities/enums";
import moment from "moment";

const ChequeDetails = ({ chequeImages, open, handleClose, initialValues }) => {
  // cheques is an array of cheque image objects, the front images have a position value of 1 while rear images have a position value of 2
  const frontImage = chequeImages.find((cheque) => cheque.position === 1);
  const rearImage = chequeImages.find((cheque) => cheque.position === 2);
  return (
    <Dialog open={open} aria-labelledby="form-dialog-title" maxWidth="lg">
      <DialogTitle
        id="form-dialog-title"
        style={{ backgroundColor: "#4B0082", color: "#fff" }}
      >
        Cheque Details
      </DialogTitle>
      <DialogContent style={{ width: "500px" }}>
        <Grid container xs={12}>
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Date Created:
            </Grid>
            <Grid item xs={7}>
              {moment(initialValues.datePosted).format("MMM Do YYYY")}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Bank:
            </Grid>
            <Grid item xs={7}>
              {initialValues.bankName}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Account Name:{" "}
            </Grid>
            <Grid item xs={7}>
              {initialValues.accountName}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Account Number:{" "}
            </Grid>
            <Grid item xs={7}>
              {initialValues.accountNumber}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Offset Bank:{" "}
            </Grid>
            <Grid item xs={7}>
              {initialValues.offsetBankName}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Offset Account Name:{" "}
            </Grid>
            <Grid item xs={7}>
              {initialValues.offsetAccountName}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Offset Account Number:{" "}
            </Grid>
            <Grid item xs={7}>
              {initialValues.offsetAccountNumber}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Amount:{" "}
            </Grid>
            <Grid item xs={7}>{`N${monetizeAmount(
              initialValues.amount
            )}`}</Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Posted By:{" "}
            </Grid>
            <Grid item xs={7}>
              {initialValues.postedBy}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Cheque Status:{" "}
            </Grid>
            <Grid item xs={7}>
              {parseChequeStatus(initialValues.status)}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              {initialValues.status === CHEQUESTATUS.Approved
                ? "Reason for approval:"
                : "Reason for rejection:"}
            </Grid>
            <Grid item xs={7}>
              {initialValues.reason}
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12} style={{ marginTop: "10px" }}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Cheque Front View:{" "}
            </Grid>
            <Grid item xs={7} style={{ height: "100px" }}>
              <img
                alt="front cheque"
                style={{ width: "100%", height: "100%", display: "block" }}
                src={`data:${frontImage?.mimeType};base64,${frontImage?.image}`}
              />
            </Grid>
          </Grid>
          <br />
          <br />
          <Grid container xs={12} style={{ marginTop: "10px" }}>
            <Grid item xs={5} style={{ fontWeight: 700 }}>
              Cheque Rear view:{" "}
            </Grid>
            <Grid item xs={7} style={{ height: "100px" }}>
              <img
                alt="rear cheque"
                style={{ width: "100%", height: "100%", display: "block" }}
                src={`data:${rearImage?.mimeType};base64,${rearImage?.image}`}
              />
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ChequeDetails;
