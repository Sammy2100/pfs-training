import MenuAppBar from "../AppLayout";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button, TablePagination } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import {
  getUserFromLocalStorage,
  monetizeAmount,
  parseChequeStatus,
  serviceError,
} from "../../../Utilities/functions";
import ChequeFormModal from "./ChequeFormModal";
import { getBanks } from "../../../Services/bankServices";
import {
  approveTransactions,
  getTransactionsByStatus,
} from "../../../Services/transactionServices";
import { CHEQUESTATUS } from "../../../Utilities/enums";
import RejectedModal from "./PendingCheques/RejectedModal";
import ITellerAlert from "../ITellerAlert";
import moment from "moment";

//material ui customized table cell
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#4B0082",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

//material ui customized table row
const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

//material ui customized button
const ColorButton = withStyles(() => ({
  root: {
    backgroundColor: "transparent",
    border: "1px solid #4B0082",
    "&:hover": {
      backgroundColor: "gainsboro",
    },
  },
}))(Button);

//material ui customized styling accessed as an object
const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 700,
  },
  loader: {
    display: "flex",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
  },
}));

const ChequeManagement = () => {
  const classes = useStyles();
  //use React hooks to set state as variables and set new values to the variables using the setstate method
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [cheques, setCheques] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [openReject, setOpenReject] = React.useState(false);
  const [loadingData, setLoadingData] = useState(true);
  const [initialValues, setInitialValues] = useState({});
  const [banks, setBanks] = useState([]);
  const [alertMessage, setAlertMessage] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [alertSeverity, setAlertSeverity] = useState("");
  const [approving, setApproving] = useState(false);
  const user = getUserFromLocalStorage();

  const handleChequeApproval = async (transactionId) => {
    setApproving(true);
    try {
      const newVvalues = {
        transactionId: transactionId,
        operatorId: user.operatorId,
        reason: "APPROVED, Cheque is OK.",
      };
      await approveTransactions(newVvalues);
      let cheques = await getTransactionsByStatus(CHEQUESTATUS.Pending);
      setCheques(cheques.data);
      setApproving(false);
      setAlertSeverity("success");
      setShowAlert(true);
      setAlertMessage("Transaction approved successfully!");
    } catch (err) {
      setAlertSeverity("error");
      serviceError(err, setAlertMessage, setShowAlert);
      setApproving(false);
    }
  };

  const handleChequeRejection = async (transactionId) => {
    const newVvalues = {
      transactionId: transactionId,
      operatorId: user.operatorId,
      reason: "",
    };
    setInitialValues(newVvalues);
    setOpenReject(true);
  };

  //function to open the cheque details modal
  const handleClickOpen = () => {
    setOpen(true);
  };

  //function to close the cheque details modal
  const handleClose = () => {
    setOpen(false);
    setOpenReject(false);
  };

  //function to change the page on the table pagination
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  //function to change the number of rows per page on the table pagination
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  //React hook useEffect to handle calls as soon as the component mounts, hence the empty arrray
  useEffect(() => {
    const fetchData = async () => {
      try {
        let cheques = await getTransactionsByStatus(CHEQUESTATUS.Pending);
        let banks = await getBanks();
        setCheques(cheques.data);
        setBanks(banks.data);
        setLoadingData(false);
      } catch (err) {
        if (err.response && err.response.data) {
          console.log(err.response.data);
        } else {
          console.log("An exception occured!");
        }
      }
    };
    fetchData();
  }, []);

  //initial value object for a new cheque
  const newCheque = {
    accountName: "",
    accountNumber: "",
    offsetAccountName: "",
    offsetAccountNumber: "",
    bankId: "",
    offsetBankId: "",
    amount: 0,
    postedBy: user.operatorId,
    frontImage: "",
    rearImage: "",
  };

  //loop throw the cheques coming from the endpoint that was assigned to the state cheques to display each row on the table, moment used to format date instead of writing a format date function, monetize amount function was written in the utilities file to make amount value readable
  const rowsDisplayed = cheques
    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    .map((cheque, index) => {
      return (
        <StyledTableRow hover role="checkbox" tabIndex={-1} key={index}>
          <StyledTableCell>{index + 1}</StyledTableCell>
          <StyledTableCell>
            {moment(cheque.datePosted).format("MMM Do YYYY")}
          </StyledTableCell>
          <StyledTableCell>{cheque.accountName}</StyledTableCell>
          <StyledTableCell>{cheque.accountNumber}</StyledTableCell>
          <StyledTableCell>{cheque.offsetAccountName}</StyledTableCell>
          <StyledTableCell>{cheque.offsetAccountNumber}</StyledTableCell>
          <StyledTableCell>{`N${monetizeAmount(
            cheque.amount
          )}`}</StyledTableCell>
          <StyledTableCell>{cheque.postedBy}</StyledTableCell>
          <StyledTableCell>{parseChequeStatus(cheque.status)}</StyledTableCell>
          <StyledTableCell>
            <ColorButton
              disabled={approving}
              onClick={() => handleChequeApproval(cheque.id)}
              size="small"
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {" "}
              Approve
            </ColorButton>{" "}
            {"    "}
            <ColorButton
              onClick={() => handleChequeRejection(cheque.id)}
              size="small"
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {" "}
              Reject
            </ColorButton>
          </StyledTableCell>
        </StyledTableRow>
      );
    });
  return (
    <>
      {loadingData ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "100vh",
          }}
        >
          <p>
            <CircularProgress />
          </p>
        </div>
      ) : (
        <MenuAppBar>
          {(user) => {
            return (
              <>
                <div style={{ marginBottom: "40px" }}>
                  <ColorButton
                    variant="outlined"
                    color="primary"
                    onClick={() => {
                      setInitialValues(newCheque);
                      handleClickOpen();
                    }}
                  >
                    New Cheque
                  </ColorButton>
                </div>
                <ITellerAlert
                  showAlert={showAlert}
                  alertSeverity={alertSeverity}
                  alertMessage={alertMessage}
                  setShowAlert={setShowAlert}
                />
                <TableContainer component={Paper}>
                  <Table
                    className={classes.table}
                    aria-label="customized table"
                  >
                    <TableHead>
                      <TableRow>
                        <StyledTableCell>S/No.</StyledTableCell>
                        <StyledTableCell>Date Created</StyledTableCell>
                        <StyledTableCell>Account Name</StyledTableCell>
                        <StyledTableCell>Account Number</StyledTableCell>
                        <StyledTableCell>Offset Account Name</StyledTableCell>
                        <StyledTableCell>Offset Account Number</StyledTableCell>
                        <StyledTableCell>Deposit</StyledTableCell>
                        <StyledTableCell>Posted By</StyledTableCell>
                        <StyledTableCell>Cheque Status</StyledTableCell>
                        <StyledTableCell>Action</StyledTableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>{rowsDisplayed}</TableBody>
                  </Table>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, 100]}
                    component="div"
                    count={cheques.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                  />
                </TableContainer>
                <ChequeFormModal
                  setCheques={setCheques}
                  banks={banks}
                  initialValues={initialValues}
                  open={open}
                  handleClose={handleClose}
                />
                <RejectedModal
                  setAlertSeverity={setAlertSeverity}
                  setShowAlert={setShowAlert}
                  setAlertMessage={setAlertMessage}
                  initialValues={initialValues}
                  open={openReject}
                  handleClose={handleClose}
                  setCheques={setCheques}
                />
              </>
            );
          }}
        </MenuAppBar>
      )}
    </>
  );
};

export default ChequeManagement;
