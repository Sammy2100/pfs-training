import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik } from 'formik';
import * as Yup from "yup"
import { serviceError } from '../../../../../Utilities/functions';
import { declineTransactions, getTransactionsByStatus } from '../../../../../Services/transactionServices';
import { CHEQUESTATUS } from '../../../../../Utilities/enums';

const RejectedModal = ({open, handleClose, initialValues, setCheques, setAlertSeverity, setShowAlert, setAlertMessage }) => {
    const [errorMessage, setErrorMessage] = useState("");
    const [showError, setShowError] = useState(false);

    //check that the error message and show error states are returned back to false and empty after the modal has been closed
    useEffect(() => {
        if(!open) {
            setShowError(false);
            setErrorMessage('')
        }
    }, [open])
    
return (
    <div>
        <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={Yup.object().shape({})}
            onSubmit={async(values, {setSubmitting, resetForm}) => {
                try {
                   await declineTransactions(values);
                   let cheques = await getTransactionsByStatus(CHEQUESTATUS.Pending);
                    setCheques(cheques.data);
                   setAlertSeverity('success')
                   setShowAlert(true);
                   setAlertMessage('Transaction was rejected successfully!');
                    handleClose();
                } catch (err) {
                    serviceError(err, setErrorMessage, setShowError)
                  }
                setSubmitting(false);
                resetForm({values: ""})
            }}
        >
            {({values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting, setFieldValue}) => {
                return(
                    <Dialog open={open} aria-labelledby="form-dialog-title" maxWidth="lg" >
                        <DialogTitle id="form-dialog-title" style={{backgroundColor: "#4B0082", color: '#fff'}}>Reject Transaction?</DialogTitle>
                        <DialogContent style={{width: '500px'}}>
                            <TextField
                                margin="dense"
                                id="reason"
                                label="Reason"
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                name="reason"
                                value={values.reason}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                helperText={errors.reason && touched.reason && errors.reason}
                                error={errors.reason && touched.reason && errors.reason}
                            />
                            {showError && <div>
                                <p style={{color: 'red'}}>{errorMessage}</p>
                            </div>}
                        </DialogContent>
                        <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={handleSubmit} color="primary" disabled={isSubmitting}>
                            {isSubmitting ? "Rejecting..." : "Reject"}
                        </Button>
                        </DialogActions>
                    </Dialog>
                )
            }}
        </Formik>
    </div>
  );
};

export default RejectedModal;