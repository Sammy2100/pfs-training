import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Formik } from "formik";
import * as Yup from "yup";
import { serviceError } from "../../../../Utilities/functions";
import { Typography } from "@material-ui/core";
import {
  getTransactionsByStatus,
  postTransactions,
} from "../../../../Services/transactionServices";
import { CHEQUESTATUS } from "../../../../Utilities/enums";

const ChequeFormModal = ({
  banks,
  open,
  handleClose,
  initialValues,
  setCheques,
}) => {
  const [frontImage, setFrontImage] = useState(
    "https://www.asa.edu/wp-content/themes/asa/img/holder.png"
  );
  const [rearImage, setRearImage] = useState(
    "https://www.asa.edu/wp-content/themes/asa/img/holder.png"
  );
  const frontImageElement = React.useRef();
  const rearImageElement = React.useRef();
  const [errorMessage, setErrorMessage] = useState("");
  const [showError, setShowError] = useState(false);

  const bankOptions = banks.map((bank) => (
    <option value={bank.id}>{bank.name}</option>
  ));

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={Yup.object().shape({})}
        onSubmit={async (values, { setSubmitting, resetForm }) => {
          try {
            let formData = new FormData();

            formData.append("accountName", values.accountName);
            formData.append("accountNumber", values.accountNumber);
            formData.append("offsetAccountName", values.offsetAccountName);
            formData.append("offsetAccountNumber", values.offsetAccountNumber);
            formData.append("bankId", values.bankId);
            formData.append("offsetBankId", values.offsetBankId);
            formData.append("amount", values.amount);
            formData.append("postedBy", values.postedBy);
            formData.append("frontImage", values.frontImage);
            formData.append("rearImage", values.rearImage);

            await postTransactions(formData);
            let cheques = await getTransactionsByStatus(CHEQUESTATUS.Pending);
            setCheques(cheques.data);
            handleClose();
            setSubmitting(false);
            resetForm({ values: "" });
          } catch (err) {
            serviceError(err, setErrorMessage, setShowError);
          }
          setSubmitting(false);
          resetForm({ values: "" });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => {
          return (
            <Dialog
              open={open}
              aria-labelledby="form-dialog-title"
              maxWidth="lg"
            >
              <DialogTitle
                id="form-dialog-title"
                style={{ backgroundColor: "#4B0082", color: "#fff" }}
              >
                Create Cheque
              </DialogTitle>
              <DialogContent style={{ width: "500px" }}>
                <TextField
                  margin="dense"
                  id="bankId"
                  label="Bank"
                  select
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  name="bankId"
                  value={values.bankId}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={errors.bankId && touched.bankId && errors.bankId}
                  error={errors.bankId && touched.bankId && errors.bankId}
                >
                  <option></option>
                  {bankOptions}
                </TextField>
                <TextField
                  margin="dense"
                  id="accountName"
                  label="Account Name"
                  type="text"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  name="accountName"
                  value={values.accountName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    errors.accountName &&
                    touched.accountName &&
                    errors.accountName
                  }
                  error={
                    errors.accountName &&
                    touched.accountName &&
                    errors.accountName
                  }
                />
                <TextField
                  margin="dense"
                  id="accountNumber"
                  label="Account Number"
                  type="text"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  name="accountNumber"
                  value={values.accountNumber}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    errors.accountNumber &&
                    touched.accountNumber &&
                    errors.accountNumber
                  }
                  error={
                    errors.accountNumber &&
                    touched.accountNumber &&
                    errors.accountNumber
                  }
                />
                <TextField
                  margin="dense"
                  id="offsetBankId"
                  label="Offset Bank"
                  select
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  name="offsetBankId"
                  value={values.offsetBankId}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    errors.offsetBankId &&
                    touched.offsetBankId &&
                    errors.offsetBankId
                  }
                  error={
                    errors.offsetBankId &&
                    touched.offsetBankId &&
                    errors.offsetBankId
                  }
                >
                  <option></option>
                  {bankOptions}
                </TextField>
                <TextField
                  margin="dense"
                  id="offsetAccountName"
                  label="Offset Account Name"
                  type="text"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  name="offsetAccountName"
                  value={values.offsetAccountName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    errors.offsetAccountName &&
                    touched.offsetAccountName &&
                    errors.offsetAccountName
                  }
                  error={
                    errors.offsetAccountName &&
                    touched.offsetAccountName &&
                    errors.offsetAccountName
                  }
                />
                <TextField
                  margin="dense"
                  id="offsetAccountNumber"
                  label="Offset Account Number"
                  type="text"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  name="offsetAccountNumber"
                  value={values.offsetAccountNumber}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    errors.offsetAccountNumber &&
                    touched.offsetAccountNumber &&
                    errors.offsetAccountNumber
                  }
                  error={
                    errors.offsetAccountNumber &&
                    touched.offsetAccountNumber &&
                    errors.offsetAccountNumber
                  }
                />
                <TextField
                  margin="dense"
                  id="amount"
                  label="Amount"
                  type="number"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  name="amount"
                  value={values.amount}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={errors.amount && touched.amount && errors.amount}
                  error={errors.amount && touched.amount && errors.amount}
                />
                <div>
                  <Typography>Upload Front Image</Typography>
                  <div
                    style={{
                      width: "100%",
                      height: "200px",
                      margin: "0px auto",
                    }}
                  >
                    <img
                      src={frontImage}
                      style={{
                        display: "block",
                        width: "100%",
                        height: "100%",
                      }}
                      alt="..."
                      className="profile-pic img-thumbnail"
                      onClick={() => {
                        rearImageElement.current.click();
                      }}
                    />
                  </div>
                  {errors?.frontImage && touched?.frontImage && (
                    <div className="small text-danger">{errors.frontImage}</div>
                  )}
                  <input
                    type="file"
                    accept="image/*"
                    ref={rearImageElement}
                    id="frontImage"
                    name="frontImage"
                    style={{ display: "none" }}
                    onChange={(event) => {
                      let uploadedImage = event.target.files[0];
                      let reader = new FileReader();
                      reader.onloadend = () => setFrontImage(reader.result);
                      reader.readAsDataURL(uploadedImage);
                      setFrontImage(reader.result);
                      return setFieldValue("frontImage", uploadedImage);
                    }}
                  />
                </div>
                <div>
                  <Typography>Upload Rear Image</Typography>
                  <div
                    style={{
                      width: "100%",
                      height: "200px",
                      margin: "0px auto",
                    }}
                  >
                    <img
                      src={rearImage}
                      style={{
                        display: "block",
                        width: "100%",
                        height: "100%",
                      }}
                      alt="..."
                      className="profile-pic img-thumbnail"
                      onClick={() => {
                        frontImageElement.current.click();
                      }}
                    />
                  </div>
                  {errors?.rearImage && touched?.rearImage && (
                    <div className="small text-danger">{errors.rearImage}</div>
                  )}
                  <input
                    type="file"
                    accept="image/*"
                    ref={frontImageElement}
                    id="frontImage"
                    name="frontImage"
                    style={{ display: "none" }}
                    onChange={(event) => {
                      let uploadedImage = event.target.files[0];
                      let reader = new FileReader();
                      reader.onloadend = () => setRearImage(reader.result);
                      reader.readAsDataURL(uploadedImage);
                      setRearImage(reader.result);
                      return setFieldValue("rearImage", uploadedImage);
                    }}
                  />
                </div>
                {showError && (
                  <div>
                    <p>{errorMessage}</p>
                  </div>
                )}
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary">
                  Cancel
                </Button>
                <Button
                  onClick={handleSubmit}
                  color="primary"
                  disabled={isSubmitting}
                >
                  {isSubmitting ? "Submitting..." : "Submit"}
                </Button>
              </DialogActions>
            </Dialog>
          );
        }}
      </Formik>
    </div>
  );
};

export default ChequeFormModal;
