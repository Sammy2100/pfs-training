import React, { useEffect } from "react";
import { Alert } from "@material-ui/lab";

const ITellerAlert = (props) => {
  useEffect(
    (props) => {
      if (props.showAlert) {
        setTimeout(() => {
          props.setShowAlert(false);
        }, 10000);
      }
    },
    [props.showAlert]
  );
  return (
    <>
      {props.showAlert && (
        <Alert severity={props.alertSeverity}>{props.alertMessage}</Alert>
      )}
    </>
  );
};

export default ITellerAlert;
