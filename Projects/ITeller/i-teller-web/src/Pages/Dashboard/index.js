import MenuAppBar from "./AppLayout";

const Dashboard = () => {
  return (
    <MenuAppBar>
      {(user) => {
        return <h1>Hello {user.fullname}</h1>;
      }}
    </MenuAppBar>
  );
};

export default Dashboard;
