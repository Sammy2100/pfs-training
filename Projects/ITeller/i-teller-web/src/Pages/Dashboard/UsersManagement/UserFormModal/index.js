import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik } from 'formik';
import * as Yup from "yup"
import { registerUser, updateUser } from '../../../../Services/userServices';
import { serviceError } from '../../../../Utilities/functions';
import { getPeopleViewModel } from '../../../../Services/personServices';
import { OPERATORTYPE } from '../../../../Utilities/enums';

const UserFormModal = ({setUsers, open, handleClose, initialValues,  setAlertSeverity, setShowAlert,setAlertMessage}) => {
    const [errorMessage, setErrorMessage] = useState("");
    const [showError, setShowError] = useState(false);
  return (
    <div>
        <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={Yup.object().shape(initialValues.userID ? {
                emailAddress: Yup.string().email("Invalid email address format").required('Please provide user email address'),
                surname: Yup.string().required('Please provide user surname'),
                firstname: Yup.string().required('Please provide user firstname'),
                othername: Yup.string(),
                dateOfBirth: Yup.date().required('Please provide user date of birth'),
            } : {
                emailAddress: Yup.string().email("Invalid email address format").required('Please provide user email address'),
                surname: Yup.string().required('Please provide user surname'),
                firstname: Yup.string().required('Please provide user firstname'),
                othername: Yup.string(),
                dateOfBirth: Yup.date().required('Please provide user date of birth'),
                operatorType: Yup.number().min(1, 'Please select an operator type').required('Please select an operator type'),
            })}
            onSubmit={async(values, {setSubmitting, resetForm}) => {
                try {
                    if(values.userID) {
                        await updateUser(values);
                    } else {
                        await registerUser(values)
                    }
                    let response = await getPeopleViewModel();
                    setUsers(response.data);
                    setAlertSeverity('success')
                   setShowAlert(true);
                   setAlertMessage(values.userID ? 'User record updated successfully!': 'User created successfully!');
                    handleClose();
                } catch (err) {
                    serviceError(err, setErrorMessage, setShowError)
                }
                setSubmitting(false);
                resetForm({values: ""})
            }}
        >
            {({values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting}) => {
                return(
                    <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth="lg" >
                        <DialogTitle id="form-dialog-title" style={{backgroundColor: "#4B0082", color: '#fff'}}>{initialValues.userID ? "Edit User" : "Create User"}</DialogTitle>
                        <DialogContent style={{width: '500px'}}>
                            <TextField
                                margin="dense"
                                id="email"
                                label="Email Address"
                                type="email"
                                variant="outlined"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                readOnly={values.userID}
                                name="emailAddress"
                                value={values.emailAddress}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                helperText={errors.emailAddress && touched.emailAddress && errors.emailAddress}
                                error={errors.emailAddress && touched.emailAddress && errors.emailAddress}
                            />
                            <TextField
                                margin="dense"
                                id="firstname"
                                label="First Name"
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                name="firstname"
                                value={values.firstname}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                helperText={errors.firstname && touched.firstname && errors.firstname}
                                error={errors.firstname && touched.firstname && errors.firstname}
                            />
                            <TextField
                                margin="dense"
                                id="lastname"
                                label="Last Name"
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                name="surname"
                                value={values.surname}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                helperText={errors.surname && touched.surname && errors.surname}
                                error={errors.surname && touched.surname && errors.surname}
                            />
                            <TextField
                                margin="dense"
                                id="othername"
                                label="Other Name"
                                type="text"
                                variant="outlined"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                name="othername"
                                value={values.othername}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                helperText={errors.othername && touched.othername && errors.othername}
                                error={errors.othername && touched.othername && errors.othername}
                            />
                            <TextField
                                margin="dense"
                                id="dateOfBirth"
                                label="Date of Birth"
                                type="date"
                                variant="outlined"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                name="dateOfBirth"
                                value={values.dateOfBirth}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                helperText={errors.dateOfBirth && touched.dateOfBirth && errors.dateOfBirth}
                                error={errors.dateOfBirth && touched.dateOfBirth && errors.dateOfBirth}
                            />
                            {!values.userID && <TextField
                                margin="dense"
                                id="operatorType"
                                label="Operator Type"
                                select
                                variant="outlined"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                name="operatorType"
                                value={values.operatorType}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                helperText={errors.operatorType && touched.operatorType && errors.operatorType}
                                error={errors.operatorType && touched.operatorType && errors.operatorType}
                            >
                                <option value={0}></option>
                                <option value={OPERATORTYPE.Administrator}>Administrator</option>
                                <option value={OPERATORTYPE.Operator}>Operator</option>
                            </TextField>}
                            {showError && <div>
                                <p>{errorMessage}</p>
                            </div>}
                        </DialogContent>
                        <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={handleSubmit} color="primary" disabled={isSubmitting}>
                            {isSubmitting ? "Submitting..." : "Submit"}
                        </Button>
                        </DialogActions>
                    </Dialog>
                )
            }}
        </Formik>
    </div>
  );
};

export default UserFormModal;