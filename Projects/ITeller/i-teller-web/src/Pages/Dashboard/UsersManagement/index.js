import MenuAppBar from "../AppLayout";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button, TablePagination } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import React, { useEffect, useState } from "react";
import UserFormModal from "./UserFormModal";
import CircularProgress from "@material-ui/core/CircularProgress";
import { getPeopleViewModel } from "../../../Services/personServices";
import { formatDate } from "../../../Utilities/functions";
import moment from "moment";
import ITellerAlert from "../ITellerAlert";

//material ui customized table cell
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#4B0082",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

//material ui customized table row
const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

//material ui customized button
const ColorButton = withStyles(() => ({
  root: {
    backgroundColor: "transparent",
    border: "1px solid #4B0082",
    "&:hover": {
      backgroundColor: "gainsboro",
    },
  },
}))(Button);

//material ui customized styling accessed as an object
const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 700,
  },
  loader: {
    display: "flex",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
  },
}));

//initial value object for a new user
const newUser = {
  emailAddress: "",
  surname: "",
  firstname: "",
  othername: "",
  dateOfBirth: "",
  operatorType: 0,
};

const UserManagement = (props) => {
  const classes = useStyles();
  //use React hooks to set state as variables and set new values to the variables using the setstate method
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [users, setUsers] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [loadingData, setLoadingData] = useState(true);
  const [initialValues, setInitialValues] = useState({});
  const [alertMessage, setAlertMessage] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [alertSeverity, setAlertSeverity] = useState("");

  //function to open the cheque details modal
  const handleClickOpen = () => {
    setOpen(true);
  };

  //function to close the cheque details modal
  const handleClose = () => {
    setOpen(false);
  };

  //function to change the page on the table pagination
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  //function to change the number of rows per page on the table pagination
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  //React hook useEffect to handle calls as soon as the component mounts, hence the empty arrray
  useEffect(() => {
    (async () => {
      try {
        let res = await getPeopleViewModel();
        setUsers(res.data);
        setLoadingData(false);
      } catch (err) {
        if (err.response && err.response.data) {
          console.log(err.response.data);
        } else {
          console.log("An exception occured!");
        }
      }
    })();
  }, []);

  //loop throw the cheques coming from the endpoint that was assigned to the state cheques to display each row on the table, moment used to format date instead of writing a format date function, monetize amount function was written in the utilities file to make amount value readable
  const rowsDisplayed = users
    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    .map((user, index) => {
      return (
        <StyledTableRow hover role="checkbox" tabIndex={-1} key={index}>
          <StyledTableCell>{index + 1}</StyledTableCell>
          <StyledTableCell>{user.emailAddress}</StyledTableCell>
          <StyledTableCell>{user.firstname}</StyledTableCell>
          <StyledTableCell>{user.surname}</StyledTableCell>
          <StyledTableCell>{user.othername}</StyledTableCell>
          <StyledTableCell>
            {moment(user.dateOfBirth).format("MMM Do YYYY")}
          </StyledTableCell>
          <StyledTableCell>
            <ColorButton
              onClick={async () => {
                setInitialValues({
                  ...user,
                  dateOfBirth: formatDate(user.dateOfBirth),
                });
                handleClickOpen();
              }}
              size="small"
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <EditIcon fontSize="small" /> Edit
            </ColorButton>
          </StyledTableCell>
        </StyledTableRow>
      );
    });
  return (
    <>
      <MenuAppBar>
        {(user) => {
          return (
            <>
              {loadingData ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    height: "50vh",
                  }}
                >
                  <p>
                    <CircularProgress />
                  </p>
                </div>
              ) : (
                <>
                  <div style={{ marginBottom: "40px" }}>
                    <ColorButton
                      variant="outlined"
                      color="primary"
                      onClick={() => {
                        setInitialValues(newUser);
                        handleClickOpen();
                      }}
                    >
                      New User
                    </ColorButton>
                  </div>
                  <ITellerAlert
                    showAlert={showAlert}
                    alertSeverity={alertSeverity}
                    alertMessage={alertMessage}
                    setShowAlert={setShowAlert}
                  />
                  <TableContainer component={Paper}>
                    <Table
                      className={classes.table}
                      aria-label="customized table"
                    >
                      <TableHead>
                        <TableRow>
                          <StyledTableCell>S/No.</StyledTableCell>
                          <StyledTableCell>Email Address</StyledTableCell>
                          <StyledTableCell>First Name</StyledTableCell>
                          <StyledTableCell>Last Name</StyledTableCell>
                          <StyledTableCell>Middle Name</StyledTableCell>
                          <StyledTableCell>Date of Birth</StyledTableCell>
                          <StyledTableCell>Action</StyledTableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>{rowsDisplayed}</TableBody>
                    </Table>
                    <TablePagination
                      rowsPerPageOptions={[5, 10, 25, 100]}
                      component="div"
                      count={users.length}
                      rowsPerPage={rowsPerPage}
                      page={page}
                      onChangePage={handleChangePage}
                      onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                  </TableContainer>
                  <UserFormModal
                    setUsers={setUsers}
                    setAlertSeverity={setAlertSeverity}
                    setShowAlert={setShowAlert}
                    setAlertMessage={setAlertMessage}
                    initialValues={initialValues}
                    open={open}
                    handleClose={handleClose}
                  />
                </>
              )}
            </>
          );
        }}
      </MenuAppBar>
    </>
  );
};

export default UserManagement;
