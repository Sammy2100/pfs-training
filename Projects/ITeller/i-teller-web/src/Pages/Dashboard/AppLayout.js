import React from "react";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import TreeView from "@material-ui/lab/TreeView";
import TreeItem from "@material-ui/lab/TreeItem";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import GroupIcon from "@material-ui/icons/Group";
import LocalAtmIcon from "@material-ui/icons/LocalAtm";
import PostAddIcon from "@material-ui/icons/PostAdd";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import DoneAllIcon from "@material-ui/icons/DoneAll";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import ThumbDownIcon from "@material-ui/icons/ThumbDown";
import DashboardIcon from "@material-ui/icons/Dashboard";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { Divider } from "@material-ui/core";
import { getUserFromLocalStorage, logout } from "../../Utilities/functions";
import { useHistory } from "react-router";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexGrow: 1,
    color: "#4B0082",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  title: {
    flexGrow: 1,
  },
  labelRoot: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(2, 0),
  },
  labelIcon: {
    marginRight: theme.spacing(1),
  },
  labelText: {
    fontWeight: "inherit",
    flexGrow: 1,
    fontSize: "15px",
  },
}));

function AppLayout(props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const user = getUserFromLocalStorage();
  const history = useHistory();

  const open = Boolean(anchorEl);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div style={{ color: "#4B0082" }}>
      <div className={classes.toolbar} />
      <Divider />
      <TreeView
        defaultExpanded={["2"]}
        defaultCollapseIcon={<ArrowDropDownIcon />}
        defaultExpandIcon={<ArrowRightIcon />}
        defaultEndIcon={<div style={{ width: 24 }} />}
      >
        <TreeItem
          onClick={() => history.push("/dashboard")}
          nodeId="8"
          label={
            <div className={classes.labelRoot}>
              <DashboardIcon color="inherit" className={classes.labelIcon} />
              <Typography variant="body2" className={classes.labelText}>
                Dashboard
              </Typography>
            </div>
          }
        />
        <TreeItem
          onClick={() => history.push("/users")}
          nodeId="1"
          label={
            <div className={classes.labelRoot}>
              <GroupIcon color="inherit" className={classes.labelIcon} />
              <Typography variant="body2" className={classes.labelText}>
                User Management
              </Typography>
            </div>
          }
        />
        <TreeItem
          nodeId="2"
          label={
            <div className={classes.labelRoot}>
              <LocalAtmIcon color="inherit" className={classes.labelIcon} />
              <Typography variant="body2" className={classes.labelText}>
                Cheque Management
              </Typography>
            </div>
          }
        >
          <TreeItem
            nodeId="3"
            onClick={() => history.push("/cheques")}
            label={
              <div className={classes.labelRoot}>
                <PostAddIcon color="inherit" className={classes.labelIcon} />
                <Typography variant="body2" className={classes.labelText}>
                  Posting
                </Typography>
              </div>
            }
          />
          <TreeItem
            nodeId="4"
            label={
              <div className={classes.labelRoot}>
                <VerifiedUserIcon
                  color="inherit"
                  className={classes.labelIcon}
                />
                <Typography variant="body2" className={classes.labelText}>
                  Approvals
                </Typography>
              </div>
            }
          >
            <TreeItem
              nodeId="5"
              onClick={() => history.push("/pending-cheques")}
              label={
                <div className={classes.labelRoot}>
                  <HourglassEmptyIcon
                    color="inherit"
                    className={classes.labelIcon}
                  />
                  <Typography variant="body2" className={classes.labelText}>
                    Pending
                  </Typography>
                </div>
              }
            />
            <TreeItem
              nodeId="6"
              onClick={() => history.push("/approved-cheques")}
              label={
                <div className={classes.labelRoot}>
                  <DoneAllIcon color="inherit" className={classes.labelIcon} />
                  <Typography variant="body2" className={classes.labelText}>
                    Approved
                  </Typography>
                </div>
              }
            />
            <TreeItem
              nodeId="7"
              onClick={() => history.push("/rejected-cheques")}
              label={
                <div className={classes.labelRoot}>
                  <ThumbDownIcon
                    color="inherit"
                    className={classes.labelIcon}
                  />
                  <Typography variant="body2" className={classes.labelText}>
                    Rejected
                  </Typography>
                </div>
              }
            />
          </TreeItem>
        </TreeItem>
      </TreeView>
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" color="inherit" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            <span className="iTellerNavbarText">I -Teller</span>
          </Typography>
          <div>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={open}
              onClose={handleClose}
            >
              <MenuItem onClick={handleClose}>{user.emailAddress}</MenuItem>
              <Divider />
              <MenuItem onClick={logout}>Logout</MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {props.children(user)}
      </main>
    </div>
  );
}

export default AppLayout;
