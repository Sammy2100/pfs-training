import { useEffect } from 'react';

const Home = () => {
    useEffect(() => {
        window.location = "/login"
    }, [])

    return(
        <div></div>
    )
};

export default Home;