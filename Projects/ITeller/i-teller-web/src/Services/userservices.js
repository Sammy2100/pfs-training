import http from "./httpServices";
import { apiUrl } from "../config.json";
const userUrl = apiUrl + "User";

export function loginUser(form) {
  return http.post(userUrl + '/Login', form);
}

export function registerUser(form) {
  return http.post(userUrl + `/Register`, form);
}

export function updateUser(form) {
  return http.put(userUrl + `/${form.userID}`, form);
}

export function getUsers() {
  return http.get(userUrl);
}

export function getUserById(id) {
  return http.get(userUrl + `/${id}`);
}