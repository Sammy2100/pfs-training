import http from "./httpServices";
import { apiUrl } from "../config.json";
const transactionUrl = apiUrl + "Transaction";

export function postTransactions(values) {
  return http.post(transactionUrl + '/PostCheque', values);
}

export function getTransactionsByStatus(status) {
    return http.get(transactionUrl + `/GetByStatus/${status}`);
}

export function getTransactionsByOperatorId(operatorId) {
    return http.get(transactionUrl + `/GetByOperatorID/${operatorId}`);
}

export function approveTransactions(values) {
  return http.put(transactionUrl + `/Approve`, values);
}

export function declineTransactions(values) {
  return http.put(transactionUrl + `/Decline`, values);
}