import http from "./httpServices";
import { apiUrl } from "../config.json";
const accountUrl = apiUrl + "Account";

export function getAccounts() {
  return http.get(accountUrl);
}