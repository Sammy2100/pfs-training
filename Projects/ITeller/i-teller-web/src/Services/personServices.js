import http from "./httpServices";
import { apiUrl } from "../config.json";
const personUrl = apiUrl + "Person";

export function getPeople() {
  return http.get(personUrl);
}

export function getPeopleViewModel() {
  return http.get(personUrl + `/GetUserPerson`);
}


export function getPersonById(id) {
  return http.get(personUrl +`/${id}`);
}