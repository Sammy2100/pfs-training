import http from "./httpServices";
import { apiUrl } from "../config.json";
const chequeUrl = apiUrl + "Cheque";

export function getChequesByTransactionId(transactionId) {
  return http.get(chequeUrl + `/GetByTransactionID/${transactionId}`);
}