import http from "./httpServices";
import { apiUrl } from "../config.json";
const bankUrl = apiUrl + "Bank";

export function getBanks() {
  return http.get(bankUrl);
}