import axios from "axios";
import { JwtConfig } from "../config.json";
import { getUserToken } from "../Utilities/functions";

axios.interceptors.response.use(null, (error) => {
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;
  if (!expectedError) {
    console.log("Logging the error", error);
    console.log("An exception occured.");
  }
  return Promise.reject(error);
});

// Add a request interceptor to have a header token bearer
axios.interceptors.request.use(function (config) {
  const bearerToken = "bearer " + getUserToken();
  config.headers[JwtConfig.AuthKey] = bearerToken;

  return config;
});

const http = {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete,
};

export default http;
