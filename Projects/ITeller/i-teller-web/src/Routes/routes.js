import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom"
import Dashboard from "../Pages/Dashboard";
import ChequeManagement from "../Pages/Dashboard/ChequeManagement";
import ApprovedCheques from "../Pages/Dashboard/ChequeManagement/ApprovedCheques";
import PendingCheques from "../Pages/Dashboard/ChequeManagement/PendingCheques";
import RejectedCheques from "../Pages/Dashboard/ChequeManagement/RejectedCheques";
import UserManagement from "../Pages/Dashboard/UsersManagement";
import Home from "../Pages/Home"
import Login from "../Pages/Login";
import { getUserFromLocalStorage } from "../Utilities/functions";

const Routes = () => {
    const user = getUserFromLocalStorage();
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    {!user ? <Home /> : <Redirect to="/dashboard" />}
                </Route>
                <Route exact path="/login">
                    {!user ? <Login /> : <Redirect to="/dashboard" />}
                </Route>
                <Route exact path="/dashboard">
                    {user ? <Dashboard /> : <Redirect to="/" />}
                </Route>
                <Route exact path="/users">
                    {user ? <UserManagement /> : <Redirect to="/" />}
                </Route>
                <Route exact path="/cheques">
                    {user ? <ChequeManagement /> : <Redirect to="/" />}
                </Route>
                <Route exact path="/pending-cheques">
                    {user ? <PendingCheques /> : <Redirect to="/" />}
                </Route>
                <Route exact path="/approved-cheques">
                    {user ? <ApprovedCheques /> : <Redirect to="/" />}
                </Route>
                <Route exact path="/rejected-cheques">
                    {user ? <RejectedCheques /> : <Redirect to="/" />}
                </Route>
            </Switch>
        </BrowserRouter>
    )
};

export default Routes;