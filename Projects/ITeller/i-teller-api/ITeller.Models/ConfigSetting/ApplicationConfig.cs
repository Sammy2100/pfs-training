﻿namespace ITeller.Models.ConfigSetting
{
    public class ApplicationConfig
    {
        public string Title { get; set; }
        public string Version { get; set; }
        public string Environment { get; set; }
        public string Dev { get; set; }
        public string Prod { get; set; }
        public string PasswordResetPage { get; set; }
    }
}
