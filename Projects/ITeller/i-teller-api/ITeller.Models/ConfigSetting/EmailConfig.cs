﻿namespace ITeller.Models.ConfigSetting
{
    public class EmailConfig
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string DisplayName { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsAsync { get; set; }
        public bool EnableSSL { get; set; }
    }
}
