﻿namespace ITeller.Models.ConfigSetting
{
    public class JwtConfig
    {
        public string AuthKey { get; set; }
        public string Secret { get; set; }
        public int ExpirationInMinutes { get; set; }
    }
}
