﻿using System;

namespace ITeller.Models
{
    public class Person
    {
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
