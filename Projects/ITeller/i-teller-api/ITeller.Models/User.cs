﻿using System;

namespace ITeller.Models
{
    public class User
    {
        public Guid ID { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string BVN { get; set; }
        public DateTime DateCreated { get; set; }

    }
}
