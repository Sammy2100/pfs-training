﻿using System;

namespace ITeller.Models
{
    public class UserVM
    {
        public Guid ID { get; set; }
        public string EmailAddress { get; set; }
        public string Name { get; set; }
    }
}
