﻿using System;

namespace ITeller.Models
{
    public class UserSession
    {
        public Guid UserID { get; set; }
        public Guid PersonID { get; set; }
        public Guid OperatorID { get; set; }
        public string EmailAddress { get; set; }
        public string Fullname { get; set; }
    }
}
