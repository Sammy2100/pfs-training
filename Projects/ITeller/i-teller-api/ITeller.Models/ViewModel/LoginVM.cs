﻿using System.ComponentModel.DataAnnotations;

namespace ITeller.Models
{
    public class LoginVM
    {
        [Required]
        public string EmailAddress { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
