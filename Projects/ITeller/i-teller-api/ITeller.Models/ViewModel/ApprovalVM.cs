﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ITeller.Models
{
    public class ApprovalVM
    {
        [Required]
        public Guid TransactionId { get; set; }
        [Required]
        public Guid OperatorId { get; set; }
        [Required]
        public string Reason { get; set; }
    }
}
