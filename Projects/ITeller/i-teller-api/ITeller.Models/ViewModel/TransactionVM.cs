﻿using System;

namespace ITeller.Models
{
    public class TransactionVM
    {
        public Guid ID { get; set; }
        public string Currency { get; set; }
        public string BankName { get; set; }
        public decimal Amount { get; set; }
        public string AccountName { get; set; }
        public string OffsetBankName { get; set; }
        public string OffsetAccountNumber { get; set; }
        public string OffsetAccountName { get; set; }
        public string AccountNumber { get; set; }
        public Status Status { get; set; }
        public DateTime DatePosted { get; set; }
        public string PostedBy { get; set; }
        public string Reason { get; set; }
    }
}
