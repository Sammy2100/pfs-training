﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace ITeller.Models
{
    public class PostChequeVM
    {
        [Required]
        public IFormFile FrontImage { get; set; }
        [Required]
        public IFormFile RearImage { get; set; }
        [Required]
        public string AccountName { get; set; }
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public Guid BankID { get; set; }
        [Required]
        public string OffsetAccountName { get; set; }
        [Required]
        public string OffsetAccountNumber { get; set; }
        [Required]
        public Guid OffsetBankID { get; set; }
        [Required]
        public decimal Amount { get; set; }

        public string Currency { get; set; }
        [Required]
        public Guid PostedBy { get; set; }
    }
}
