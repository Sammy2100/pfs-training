﻿using System.Collections.Generic;

namespace ITeller.Models
{
    public class Email
    {
        public string Sender { get; set; }
        public List<string> Recipients { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
        public string DisplayName { get; set; }
    }
}
