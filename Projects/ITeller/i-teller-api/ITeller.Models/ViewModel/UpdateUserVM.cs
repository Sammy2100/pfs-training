﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ITeller.Models
{
    public class UpdateUserVM
    {
        [Required]
        public Guid UserID { get; set; }
        [Required]
        public string EmailAddress { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Othername { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
    }
}
