﻿using System;

namespace ITeller.Models
{
    public class PersonUserVM
    {
        public Guid UserID { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string EmailAddress { get; set; }
    }
}
