﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ITeller.Models
{
    public class RegisterVM
    {
        [Required]
        public string EmailAddress { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Firstname { get; set; }
        public string Othername { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public OperatorType OperatorType { get; set; }
    }
}
