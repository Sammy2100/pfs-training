﻿using System.Net;

namespace ITeller.Models
{
    public class ResponseMessage
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public UserSession Data { get; set; }
    }
}
