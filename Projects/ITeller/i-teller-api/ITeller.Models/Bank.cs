﻿using System;

namespace ITeller.Models
{
    public class Bank
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
