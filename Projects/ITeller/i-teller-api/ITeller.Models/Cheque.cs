﻿using System;

namespace ITeller.Models
{
    public class Cheque
    {
        public Guid ID { get; set; }
        public Guid TransactionID { get; set; }
        public string Name { get; set; }
        public string MimeType { get; set; }
        public decimal Size { get; set; }
        public byte[] Image { get; set; }
        public Position Position { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public enum Position
    {
        Front = 1,
        Rear = 2
    }
}
