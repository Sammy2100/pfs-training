﻿using System;

namespace ITeller.Models
{
    public class Account
    {
        public Guid ID { get; set; }
        public Guid BankID { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
