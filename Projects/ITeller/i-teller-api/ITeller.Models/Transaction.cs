﻿using System;

namespace ITeller.Models
{
    public class Transaction
    {
        public Guid ID { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public Guid AccountID { get; set; }
        public Guid OffsetAccountID { get; set; }
        public Status Status { get; set; }
        public DateTime DatePosted { get; set; }
        public Guid PostedBy { get; set; }
        public Guid? ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public string Reason { get; set; }
    }

    public enum Status
    {
        Pending = 1,
        Decline = 2,
        Approved = 3
    }
}
