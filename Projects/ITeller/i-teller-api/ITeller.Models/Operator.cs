﻿using System;

namespace ITeller.Models
{
    public class Operator
    {
        public Guid ID { get; set; }
        public Guid PersonID { get; set; }
        public OperatorType OperatorType { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public enum OperatorType
    {
        Administrator = 1,
        Operator = 2
    }
}
