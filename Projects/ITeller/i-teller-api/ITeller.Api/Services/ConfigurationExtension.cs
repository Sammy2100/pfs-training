﻿using ITeller.Business;
using ITeller.Data;
using ITeller.Models.ConfigSetting;
using ITeller.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ITeller.Api
{
    public static class ConfigurationExtension
    {
        private static readonly string Prod = "Prod";
        private static readonly string AppConfig = "Config:Application";
        private static readonly string JwtConfig = "Config:Jwt";
        private static readonly string EmailConfig = "Config:Email";

        public static void ConfigurationSetings(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<ApplicationConfig>(configuration.GetSection(AppConfig));
            services.Configure<EmailConfig>(configuration.GetSection(EmailConfig));
            services.Configure<JwtConfig>(configuration.GetSection(JwtConfig));
        }

        public static void ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            var _applicationConfig = configuration.GetSection(AppConfig).Get<ApplicationConfig>();
            string databaseConnectionString = nameof(iTellerContext) + "-Prod";

            if (_applicationConfig.Environment != Prod)
                databaseConnectionString = nameof(iTellerContext);

            services.AddDbContext<iTellerContext>(
               o => o.UseSqlServer(configuration.GetConnectionString(databaseConnectionString)
            ));
        }

        public static void UseSwagger(this IApplicationBuilder app, ApplicationConfig configuration)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{configuration.Title} {configuration.Version}");
                c.RoutePrefix = string.Empty;
                c.DisplayRequestDuration();
            });
        }

        public static void ConfigureSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            var _applicationConfig = configuration.GetSection(AppConfig).Get<ApplicationConfig>();
            var _jwtConfig = configuration.GetSection(JwtConfig).Get<JwtConfig>();

            if (_applicationConfig.Environment != Prod)
            {
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc(_applicationConfig.Version, new OpenApiInfo
                    {
                        Title = _applicationConfig.Title,
                        Version = _applicationConfig.Version,
                        Contact = new OpenApiContact { Name = "Samuel Iyomere" }
                    });
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        Name = _jwtConfig.AuthKey,
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Scheme = "Bearer",
                        BearerFormat = "JWT",
                        Description = "Input your Bearer token in this format - Bearer {your token here} to access this API",
                    });
                    c.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer",
                                },
                                Scheme = "Bearer",
                                Name = "Bearer",
                                In = ParameterLocation.Header,
                            }, new List<string>()
                        },
                    });
                });
            }
        }

        public static void ConfigureAuthenticationBearer(this IServiceCollection services, JwtConfig configuration)
        {
            bool onAuthenticationFailedCalled = false;
            string authError = string.Empty, jsonContentType = "application/json";

            services.AddAuthentication(options =>
            {
                // set to bearer
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            //.AddScheme<TokenAuthenticationOptions, BearerAuthenticationHandler>(JwtBearerDefaults.AuthenticationScheme, o => { });
            .AddJwtBearer(o =>
            {
                o.RequireHttpsMetadata = false;
                o.SaveToken = false;
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero,
                    //ValidIssuer = settings.Issuer,
                    //ValidAudience = settings.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.Secret))
                };
                o.Events = new JwtBearerEvents()
                {
                    OnMessageReceived = async context =>
                    {
                        if (context.Request.Path.Value.ToLower().Contains("/user/login") &&
                            context.Request.Headers.ContainsKey(configuration.AuthKey))
                            context.Request.Headers.Remove(configuration.AuthKey);

                        // read token from authorization header
                        context.Request.Headers.TryGetValue(configuration.AuthKey, out StringValues authTokenKey);
                        if (string.IsNullOrEmpty(authTokenKey)) return;

                        // ensure its a Bearer token
                        var value = authTokenKey.ToString().Split(' ');
                        if (value[0].ToLower() != JwtBearerDefaults.AuthenticationScheme.ToLower()) return;
                        if (string.IsNullOrEmpty(value[1])) return;

                        var claimsPrincipal = JWTService.ValidateToken(value[1], configuration, out SecurityToken principal);
                        if (claimsPrincipal == null) return;
                        context.HttpContext.User = claimsPrincipal;
                    },
                    OnAuthenticationFailed = context =>
                    {
                        context.NoResult();
                        string err = context.Exception.Message;
                        if (context.Exception.GetType() == typeof(SecurityTokenValidationException))
                            err = "Invalid Token";
                        else if (context.Exception.GetType() == typeof(SecurityTokenInvalidIssuerException))
                            err = "Invalid Issuer";
                        else if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                            err = "Token Expired";
                        else if (context.Exception.GetType() == typeof(SecurityTokenException) ||
                                 context.Exception.GetType() == typeof(SecurityTokenValidationException) ||
                                 context.Exception.GetType() == typeof(SecurityTokenInvalidSignatureException))
                            err = "Authorization has been denied for this request - Invalid or Expired Token.";


                        context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        context.Response.ContentType = jsonContentType;
                        string authError = JsonConvert.SerializeObject("Unauthorized: " + err); // JsonConvert.SerializeObject(ApiResponse.Failure("Unauthorized: " + err));
                        onAuthenticationFailedCalled = true;
                        return context.Response.WriteAsync(authError);
                    },
                    OnChallenge = context =>
                    {
                        if (onAuthenticationFailedCalled)
                        {
                            authError = string.Empty;
                            onAuthenticationFailedCalled = false;
                            context.HandleResponse();
                            return context.Response.WriteAsync(authError);
                        }

                        string err = string.Empty;
                        if (!(context.AuthenticateFailure is null))
                            err = "Invalid Token";
                        else if (context.Error is null)
                            err = "Missing Token";

                        context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        context.HandleResponse();
                        context.Response.ContentType = jsonContentType;
                        authError = JsonConvert.SerializeObject("Unauthorized: " + err); // JsonConvert.SerializeObject(ApiResponse.Failure("Unauthorized: " + err));
                        return context.Response.WriteAsync(authError);
                    },
                    OnForbidden = context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        context.Response.ContentType = jsonContentType;
                        var result = JsonConvert.SerializeObject("Forbidden: Error"); // JsonConvert.SerializeObject(ApiResponse.Failure(StaticMessages.Forbidden));
                        return context.Response.WriteAsync(result);
                    }
                };
            });

            services.AddAuthorization();
        }

        public static void InjectServices(this IServiceCollection services)
        {
            services

               .AddTransient<EncryptionService>()
               .AddTransient<JWTService>()
               .AddTransient<BvnService>()

               .AddTransient<IUnitOfWork, UnitOfWork>()
               .AddTransient(typeof(IGeneric<>), typeof(GenericRepository<>))

               .AddTransient<IUser, UserRepository>().AddTransient<UserBusiness>()
               .AddTransient<IPerson, PersonRepository>().AddTransient<PersonBusiness>()
               .AddTransient<IAccount, AccountRepository>().AddTransient<AccountBusiness>()
               .AddTransient<IOperator, OperatorRepository>().AddTransient<OperatorBusiness>()
               .AddTransient<IBank, BankRepository>().AddTransient<BankBusiness>()
               .AddTransient<ITransaction, TransactionRepository>().AddTransient<TransactionBusiness>()
               .AddTransient<ICheque, ChequeRepository>().AddTransient<ChequeBusiness>();
        }
    }
}
