﻿using ITeller.Models;
using ITeller.Models.ConfigSetting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ITeller.Api
{
    public class JWTService
    {
        private JwtConfig _jwtConfig;

        public JWTService(IOptions<JwtConfig> jwtConfig)
        {
            _jwtConfig = jwtConfig.Value;
        }

        public string GenerateSecurityToken(UserSession userSession)
        {
            var subject = new ClaimsIdentity(new[]
            {
                new Claim("userId", userSession.UserID.ToString()),
                new Claim("personId", userSession.PersonID.ToString()),
                new Claim("operatorId", userSession.OperatorID.ToString()),
                new Claim("emailAddress", userSession.EmailAddress.ToString(), ClaimValueTypes.Email),
                new Claim("fullname", userSession.Fullname.ToString()),
            });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(subject),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static ClaimsPrincipal ValidateToken(string token, JwtConfig jwtConfig, out SecurityToken validatedToken)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(token))
                    throw new ArgumentNullException("Token is required");

                var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfig.Secret));
                var tokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateLifetime = true,
                    //ValidAudience = jwtConfig.Issuer,
                    //ValidIssuer = jwtConfig.Issuer,
                    IssuerSigningKey = signingKey,
                    //ValidAudiences = new string[] { settings.Audience }
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                if (!tokenHandler.CanReadToken(token) || !tokenHandler.CanValidateToken)
                {
                    validatedToken = null;
                    return null;
                }

                return tokenHandler.ValidateToken(token, tokenValidationParameters, out validatedToken);
            }
            catch (Exception)
            {
                validatedToken = null;
                return null;
            }
        }
    }
}
