﻿using ITeller.Business;
using ITeller.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ITeller.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    // [Authorize()]
    public class BankController : ControllerBase
    {
        private readonly BankBusiness _bankBusiness;

        public BankController(BankBusiness bankBusiness) =>
            _bankBusiness = bankBusiness;

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var banks = await _bankBusiness.GetBanks();
            return Ok(banks);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Bank>> Get(Guid id)
        {
            var bank = await _bankBusiness.GetBankByID(id);
            return Ok(bank);
        }

        [HttpPost]
        public async Task<ActionResult<Bank>> Post(Bank bank)
        {
            await _bankBusiness.Create(bank);
            return Ok(bank);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Bank>> Put(Bank bank, Guid id)
        {
            if (bank.ID != id)
                return BadRequest("Invalid record!");

            await _bankBusiness.Update(bank);
            return Ok(bank);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delate(Guid id)
        {
            await _bankBusiness.Delete(id);
            return Ok("Record deleted successfully");
        }
    }
}
