﻿using ITeller.Business;
using ITeller.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ITeller.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly TransactionBusiness _transactionBusiness;

        public TransactionController(TransactionBusiness transactionBusiness) =>
            _transactionBusiness = transactionBusiness;

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var transactions = await _transactionBusiness.GetTransactions();
            return Ok(transactions);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Transaction>> Get(Guid id)
        {
            var transaction = await _transactionBusiness.GetByID(id);
            return Ok(transaction);
        }

        [HttpGet("[action]/{status}")]
        public async Task<ActionResult<TransactionVM>> GetByStatus(Status status)
        {
            var transactions = await _transactionBusiness.GetByStatus(status);
            return Ok(transactions);
        }

        [HttpGet("[action]/{operatorId}")]
        public async Task<ActionResult<Transaction>> GetByOperatorID(Guid operatorId)
        {
            var transaction = await _transactionBusiness.GetByOperatorID(operatorId);
            return Ok(transaction);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> PostCheque([FromForm] PostChequeVM postCheque)
        {
            await _transactionBusiness.PostCheque(postCheque);
            return Ok("Cheque posted successfully");
        }

        [HttpPut("[action]")]
        public async Task<ActionResult> Approve(ApprovalVM approval)
        {
            var result = await _transactionBusiness.Approve(approval);

            if (result == null)
                return NotFound("This cheque is not available for approval");

            return Ok("Cheque approved successfully");
        }

        [HttpPut("[action]")]
        public async Task<ActionResult> Decline([FromBody] ApprovalVM approval)
        {
            var result = await _transactionBusiness.Decline(approval);

            if (result == null)
                return NotFound("This cheque is not available for approval");

            return Ok("Cheque declined successfully");
        }
    }
}
