﻿using ITeller.Business;
using ITeller.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ITeller.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ChequeController : ControllerBase
    {
        private readonly ChequeBusiness _chequeBusiness;

        public ChequeController(ChequeBusiness chequeBusiness) =>
            _chequeBusiness = chequeBusiness;

        [HttpGet("{id}")]
        public async Task<ActionResult<Cheque>> Get(Guid id)
        {
            var cheque = await _chequeBusiness.GetChequeByID(id);
            return Ok(cheque);
        }

        [HttpGet("[action]/{transactionId}")]
        public async Task<ActionResult> GetByTransactionID(Guid transactionId)
        {
            var cheques = await _chequeBusiness.GetByTransactionID(transactionId);
            return Ok(cheques);
        }
    }
}
