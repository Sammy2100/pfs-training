﻿using ITeller.Business;
using ITeller.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ITeller.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AccountBusiness _accountBusiness;

        public AccountController(AccountBusiness accountBusiness) =>
            _accountBusiness = accountBusiness;

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var accounts = await _accountBusiness.GetAccounts();
            return Ok(accounts);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Account>> Get(Guid id)
        {
            var account = await _accountBusiness.GetAccountByID(id);
            return Ok(account);
        }

        [HttpPost]
        public async Task<ActionResult<Account>> Post(Account account)
        {
            await _accountBusiness.Create(account);
            return Ok(account);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Account>> Put(Account account, Guid id)
        {
            if (account.ID != id)
                return BadRequest("Invalid record!");

            await _accountBusiness.Update(account);
            return Ok(account);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delate(Guid id)
        {
            await _accountBusiness.Delete(id);
            return Ok("Record deleted successfully");
        }
    }
}
