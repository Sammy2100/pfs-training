﻿using ITeller.Business;
using ITeller.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ITeller.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly PersonBusiness _personBusiness;

        public PersonController(PersonBusiness personBusiness) =>
            _personBusiness = personBusiness;

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var persons = await _personBusiness.GetPeople();
            return Ok(persons);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<PersonUserVM>> GetUserPerson()
        {
            var persons = await _personBusiness.GetUserPeople();
            return Ok(persons);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> Get(Guid id)
        {
            var person = await _personBusiness.GetPersonByID(id);
            return Ok(person);
        }

        [HttpPost]
        public async Task<ActionResult<Person>> Post(Person person)
        {
            await _personBusiness.Create(person);
            return Ok(person);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(Person person, Guid id)
        {
            if (person.ID != id)
                return BadRequest("Invalid record!");

            await _personBusiness.Update(person);
            return Ok(person);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delate(Guid id)
        {
            await _personBusiness.Delete(id);
            return Ok("Record deleted successfully");
        }
    }
}
