﻿using ITeller.Business;
using ITeller.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ITeller.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OperatorController : ControllerBase
    {
        private readonly OperatorBusiness _operatorBusiness;

        public OperatorController(OperatorBusiness operatorBusiness) =>
            _operatorBusiness = operatorBusiness;

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var ops = await _operatorBusiness.GetOperators();
            return Ok(ops);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Operator>> Get(Guid personId)
        {
            var op = await _operatorBusiness.GetOperatorByID(personId);
            return Ok(op);
        }

        [HttpGet("[action]/{personId}")]
        public async Task<ActionResult<Operator>> GetByPersonID(Guid personId)
        {
            var op = await _operatorBusiness.GetByPersonID(personId);
            return Ok(op);
        }

    }
}
