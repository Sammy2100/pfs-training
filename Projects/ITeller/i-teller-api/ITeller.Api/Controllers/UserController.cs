﻿using ITeller.Business;
using ITeller.Models;
using ITeller.Models.ConfigSetting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace ITeller.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserBusiness _userBusiness;
        private readonly JWTService _jwtService;
        private readonly JwtConfig _jwtConfig;

        public UserController(UserBusiness userBusiness, JWTService jWTService, IOptions<JwtConfig> jwtConfig)
        {
            _userBusiness = userBusiness;
            _jwtService = jWTService;
            _jwtConfig = jwtConfig.Value;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var users = await _userBusiness.GetUsers();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(Guid id)
        {
            var user = await _userBusiness.GetUserByID(id);
            return Ok(user);
        }

        [HttpGet("[action]/{emailAddress}")]
        public async Task<ActionResult<User>> GetUserByEmailAddress(string emailAddress)
        {
            var user = await _userBusiness.GetUserByEmail(emailAddress);
            return Ok(user);
        }

        [HttpPost("[action]")]
        [ProducesResponseType(200, Type = typeof(ResponseMessage))]
        public async Task<ActionResult> Register(RegisterVM register)
        {
            var result = await _userBusiness.Register(register);
            return Ok(result);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(400, Type = typeof(string))]
        public async Task<ActionResult<UpdateUserVM>> Put(UpdateUserVM user, Guid id)
        {
            if (user.UserID != id)
                return BadRequest("Invalid User!");

            var result = await _userBusiness.UpdateUser(user);
            if (result == null)
                return BadRequest("Invalid User!");

            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        [ProducesResponseType(401, Type = typeof(string))]
        [ProducesResponseType(200, Type = typeof(UserSession))]
        public async Task<ActionResult> Login(LoginVM login)
        {
            var result = await _userBusiness.Login(login.EmailAddress, login.Password);
            if (result == null)
                return Unauthorized("Email Address or Password is Invalid!");

            var token = _jwtService.GenerateSecurityToken(result);

            Response.Headers.Add(_jwtConfig.AuthKey, token);
            return Ok(token);
        }

    }
}
