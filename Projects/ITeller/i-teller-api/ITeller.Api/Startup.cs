using ITeller.Models.ConfigSetting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ITeller.Api
{
    public class Startup
    {
        private readonly string Prod = "Prod";
        private ApplicationConfig _applicationConfig;
        private JwtConfig _jwtConfig;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _applicationConfig = Configuration.GetSection("Config:Application").Get<ApplicationConfig>();
            _jwtConfig = configuration.GetSection("Config:Jwt").Get<JwtConfig>();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.InjectServices();
            services.ConfigurationSetings(Configuration);
            services.ConfigureDatabase(Configuration);
            services.ConfigureSwagger(Configuration);
            services.ConfigureAuthenticationBearer(_jwtConfig);
            services.AddControllers();
            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(options => options
                //.WithOrigins("http://20.55.17.177:80")
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            );

            if (env.IsDevelopment()) { }

            if (_applicationConfig.Environment != Prod)
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger(_applicationConfig);
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
