﻿using ITeller.Models;
using Microsoft.EntityFrameworkCore;

namespace ITeller.Data
{
    public class iTellerContext : DbContext
    {
        public iTellerContext(DbContextOptions<iTellerContext> options) : base(options) { }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Operator> Operators { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<Cheque> Cheques { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<UserVM> UserVMs { get; set; }
    }
}
