﻿using ITeller.Models;
using System;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public interface IUser : IGeneric<User>
    {
        Task<User> GetUserByEmail(string emailAddress);
        Task<UserSession> GetUserPersonOperatorByID(Guid ID);
    }
}
