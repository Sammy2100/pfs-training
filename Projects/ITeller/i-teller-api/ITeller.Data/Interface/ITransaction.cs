﻿using ITeller.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public interface ITransaction : IGeneric<Transaction>
    {
        Task<List<Transaction>> GetByOperatorID(Guid id);
        Task<List<TransactionVM>> GetByStatus(Status status);
    }
}
