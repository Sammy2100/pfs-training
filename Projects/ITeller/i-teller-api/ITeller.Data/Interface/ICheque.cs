﻿using ITeller.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public interface ICheque : IGeneric<Cheque>
    {
        Task<List<Cheque>> GetByTransactionID(Guid transactionId);
    }
}
