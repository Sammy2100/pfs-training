﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public interface IGeneric<T> where T : class
    {
        Task<List<T>> GetAll();
        Task<T> Find(Guid id);
        Task Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
