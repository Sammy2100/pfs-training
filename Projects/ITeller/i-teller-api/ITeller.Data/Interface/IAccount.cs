﻿using ITeller.Models;
using System;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public interface IAccount : IGeneric<Account>
    {
        Task<Account> GetAccountByAccountNumber(string accountNumber);
        Task<Account> GetAccountByID(Guid id);
    }
}
