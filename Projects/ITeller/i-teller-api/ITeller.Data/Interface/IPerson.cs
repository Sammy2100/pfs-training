﻿using ITeller.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public interface IPerson : IGeneric<Person>
    {
        Task<Person> GetPersonByUserID(Guid userId);
        Task<List<PersonUserVM>> GetUserPeople();
    }
}
