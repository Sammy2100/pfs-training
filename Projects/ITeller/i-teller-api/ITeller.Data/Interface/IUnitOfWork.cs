﻿using System;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> Commit();
        void Rollback();

        IUser Users { get; }
        IPerson People { get; }
        IAccount Accounts { get; }
        IOperator Operators { get; }
        ITransaction Transactions { get; }
        ICheque Cheques { get; }
        IBank Banks { get; }
    }
}
