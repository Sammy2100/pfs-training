﻿using ITeller.Models;
using System;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public interface IOperator : IGeneric<Operator>
    {
        Task<Operator> GetByPersonID(Guid personId);
    }
}
