﻿using ITeller.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public class UserRepository : GenericRepository<User>, IUser
    {
        public UserRepository(iTellerContext db) : base(db) { }

        public async Task<User> GetUserByEmail(string emailAddress)
        {
            return await GetOneBy(u => u.EmailAddress == emailAddress);
        }

        public async Task<UserSession> GetUserPersonOperatorByID(Guid ID)
        {
            var result = await (from u in _db.Users
                                join p in _db.People on u.ID equals p.UserID
                                join o in _db.Operators on p.ID equals o.PersonID
                                where u.ID == ID
                                select new UserSession
                                {
                                    EmailAddress = u.EmailAddress,
                                    UserID = u.ID,
                                    PersonID = p.ID,
                                    Fullname = $"{p.Firstname} {p.Surname} {p.Othername}",
                                    OperatorID = o.ID
                                }).FirstOrDefaultAsync();

            return result;
        }

        //public async Task<UserVM> GetUserByEmailAddress(string emailAddress)
        //{
        //    var param = new SqlParameter("@EmailAddress", emailAddress);
        //    var storedProc = "getUserByEmailAddress @EmailAddress";

        //    var query = _db.UserVMs.FromSqlRaw(storedProc, param);
        //    var users = query.ToList();
        //    return await query.FirstOrDefaultAsync();
        //}
    }
}
