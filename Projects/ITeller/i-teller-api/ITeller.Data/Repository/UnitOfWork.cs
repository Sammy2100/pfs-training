﻿using System.Threading.Tasks;

namespace ITeller.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly iTellerContext _db;
        public UnitOfWork(iTellerContext db, IUser users, IPerson people, IAccount accounts,
            IOperator operators, ITransaction transactions, ICheque cheques, IBank banks)
        {
            _db = db;
            Users = users;
            People = people;
            Accounts = accounts;
            Operators = operators;
            Transactions = transactions;
            Cheques = cheques;
            Banks = banks;
        }

        public IUser Users { get; }
        public IPerson People { get; }
        public IAccount Accounts { get; }
        public IOperator Operators { get; }
        public ITransaction Transactions { get; }
        public ICheque Cheques { get; }
        public IBank Banks { get; }

        public async Task<int> Commit() =>
            await _db.SaveChangesAsync();

        public void Rollback() => Dispose();

        public void Dispose() =>
            _db.DisposeAsync();
    }
}
