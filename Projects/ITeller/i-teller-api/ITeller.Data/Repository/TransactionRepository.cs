﻿using ITeller.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public class TransactionRepository : GenericRepository<Transaction>, ITransaction
    {
        public TransactionRepository(iTellerContext db) : base(db) { }

        public async Task<List<Transaction>> GetByOperatorID(Guid operatorId)
        {
            var result = await GetBy(t => t.PostedBy == operatorId);
            return result.ToList();
        }

        public async Task<List<TransactionVM>> GetByStatus(Status status)
        {
            var result = (from t in _db.Transactions
                          join a in _db.Accounts on t.AccountID equals a.ID
                          join oa in _db.Accounts on t.OffsetAccountID equals oa.ID
                          join b in _db.Banks on a.BankID equals b.ID
                          join ob in _db.Banks on oa.BankID equals ob.ID
                          join o in _db.Operators on t.PostedBy equals o.ID
                          join p in _db.People on o.PersonID equals p.ID
                          where t.Status == status
                          select new TransactionVM
                          {
                              ID = t.ID,
                              Currency = t.Currency,
                              Amount = t.Amount,
                              Status = t.Status,
                              DatePosted = t.DatePosted,
                              BankName = b.Name,
                              AccountName = a.Name,
                              AccountNumber = a.Number,
                              OffsetBankName = ob.Name,
                              OffsetAccountName = oa.Name,
                              OffsetAccountNumber = oa.Number,
                              PostedBy = $"{p.Firstname} {p.Surname} {p.Othername}",
                              Reason = t.Reason
                          });

            return await result.ToListAsync();
        }
    }
}
