﻿using ITeller.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public class PersonRepository : GenericRepository<Person>, IPerson
    {
        private readonly iTellerContext _db;
        public PersonRepository(iTellerContext db) : base(db)
        {
            _db = db;
        }

        public async Task<Person> GetPersonByUserID(Guid userId) =>
            await GetOneBy(p => p.UserID == userId);

        public async Task<List<PersonUserVM>> GetUserPeople()
        {
            var result = await (from p in _db.People
                                join u in _db.Users on p.UserID equals u.ID
                                select new PersonUserVM
                                {
                                    UserID = u.ID,
                                    EmailAddress = u.EmailAddress,
                                    Surname = p.Surname,
                                    Firstname = p.Firstname,
                                    Othername = p.Othername,
                                    DateOfBirth = p.DateOfBirth
                                }).ToListAsync();

            return result;
        }

        //public async Task<List<PersonAccountVM>> GetPeopleAccount()
        //{
        //    return await
        //        (from p in _db.People
        //         join a in _db.Accounts on p.ID equals a.PersonID
        //         join u in _db.Users on p.UserID equals u.ID
        //         select new PersonAccountVM
        //         {
        //             Surname = p.Surname,
        //             Firstname = p.Firstname,
        //             Othername = p.Othername,
        //             DateOfBirth = p.DateOfBirth,
        //             AccountName = a.Name,
        //             AccountNumber = a.Number,
        //             AccountBalance = a.Balance
        //         }).ToListAsync();
        //}

        //public async Task<PersonAccountVM> GetPersonAccountByID(Guid ID)
        //{
        //    return await
        //        (from p in _db.People
        //         join a in _db.Accounts on p.ID equals a.PersonID
        //         join u in _db.Users on p.UserID equals u.ID
        //         where p.ID == ID
        //         select new PersonAccountVM
        //         {
        //             Surname = p.Surname,
        //             Firstname = p.Firstname,
        //             Othername = p.Othername,
        //             DateOfBirth = p.DateOfBirth,
        //             AccountName = a.Name,
        //             AccountNumber = a.Number,
        //             AccountBalance = a.Balance
        //         }).FirstOrDefaultAsync();
        //}
    }
}
