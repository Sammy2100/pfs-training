﻿using ITeller.Models;
using System;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public class OperatorRepository : GenericRepository<Operator>, IOperator
    {
        public OperatorRepository(iTellerContext db) : base(db) { }

        public async Task<Operator> GetByPersonID(Guid personId)
        {
            var result = await GetOneBy(o => o.PersonID == personId);
            return result;
        }
    }
}
