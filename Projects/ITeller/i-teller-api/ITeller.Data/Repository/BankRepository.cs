﻿using ITeller.Models;

namespace ITeller.Data
{
    public class BankRepository : GenericRepository<Bank>, IBank
    {
        public BankRepository(iTellerContext db) : base(db) { }
    }
}
