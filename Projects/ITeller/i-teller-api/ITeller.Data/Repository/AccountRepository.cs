﻿using ITeller.Models;
using System;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public class AccountRepository : GenericRepository<Account>, IAccount
    {
        public AccountRepository(iTellerContext db) : base(db) { }

        public async Task<Account> GetAccountByAccountNumber(string accountNumber) =>
            await GetOneBy(r => r.Number == accountNumber);

        public async Task<Account> GetAccountByID(Guid id) =>
            await Find(id);

    }
}
