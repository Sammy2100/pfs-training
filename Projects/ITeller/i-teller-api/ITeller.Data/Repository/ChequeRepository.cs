﻿using ITeller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITeller.Data
{
    public class ChequeRepository : GenericRepository<Cheque>, ICheque
    {
        public ChequeRepository(iTellerContext db) : base(db) { }

        public async Task<List<Cheque>> GetByTransactionID(Guid transactionId)
        {
            var result = await GetBy(c => c.TransactionID == transactionId);
            return result.ToList();
        }
    }
}
