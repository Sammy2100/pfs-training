﻿using ITeller.Data;
using ITeller.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Business
{
    public class OperatorBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public OperatorBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Operator>> GetOperators() =>
            await _unitOfWork.Operators.GetAll();

        public async Task<Operator> GetOperatorByID(Guid id) =>
            await _unitOfWork.Operators.Find(id);

        public async Task<Operator> GetByPersonID(Guid personId) =>
            await _unitOfWork.Operators.GetByPersonID(personId);

        public async Task Create(Operator op)
        {
            await _unitOfWork.Operators.Create(op);
            await _unitOfWork.Commit();
        }

        public async Task Update(Operator op)
        {
            _unitOfWork.Operators.Update(op);
            await _unitOfWork.Commit();
        }

        public async Task Delete(Guid id)
        {
            var entity = await GetOperatorByID(id);
            _unitOfWork.Operators.Delete(entity);
        }


    }
}
