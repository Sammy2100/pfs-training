﻿using ITeller.Data;
using ITeller.Models;
using ITeller.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ITeller.Business
{
    public class UserBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly EncryptionService _encryptionService;
        private readonly PersonBusiness _personBusiness;
        private readonly OperatorBusiness _operatorBusiness;

        public UserBusiness(IUnitOfWork unitOfWork, EncryptionService encryptionService,
            PersonBusiness personBusiness, OperatorBusiness operatorBusiness)
        {
            _unitOfWork = unitOfWork;
            _encryptionService = encryptionService;
            _personBusiness = personBusiness;
            _operatorBusiness = operatorBusiness;
        }

        public async Task<List<User>> GetUsers() =>
            await _unitOfWork.Users.GetAll();

        public async Task<User> GetUserByID(Guid id) =>
            await _unitOfWork.Users.Find(id);

        public async Task<User> GetUserByEmail(string emailAddress)
        {
            return await _unitOfWork.Users.GetUserByEmail(emailAddress);
        }

        //public async Task<UserVM> GetUserByEmailAddress(string emailAddress)
        //{
        //    return await _userRepository.GetUserByEmailAddress(emailAddress);
        //}

        public async Task Create(User user)
        {
            //user.BVN = await _bvnService.GetBvnByPhoneNumber(user.PhoneNumber);
            await _unitOfWork.Users.Create(user);
            await _unitOfWork.Commit();
        }

        public async Task Update(User user)
        {
            _unitOfWork.Users.Update(user);
            await _unitOfWork.Commit();
        }

        public async Task Delete(Guid id)
        {
            var entity = await GetUserByID(id);
            _unitOfWork.Users.Delete(entity);
        }

        public async Task<UpdateUserVM> UpdateUser(UpdateUserVM updateUser)
        {
            var user = await GetUserByID(updateUser.UserID);
            if (user == null) return null;

            user.EmailAddress = updateUser.EmailAddress;

            var person = await _personBusiness.GetPersonByUserID(updateUser.UserID);
            person.Surname = updateUser.Surname;
            person.Firstname = updateUser.Firstname;
            person.Othername = updateUser.Othername;
            person.DateOfBirth = updateUser.DateOfBirth;

            _unitOfWork.Users.Update(user);
            _unitOfWork.People.Update(person);
            await _unitOfWork.Commit();

            return updateUser;
        }

        public async Task<ResponseMessage> Register(RegisterVM register)
        {
            var user = await GetUserByEmail(register.EmailAddress);
            if (user != null)
                return new ResponseMessage
                {
                    Data = null,
                    Message = "Email address already exist!",
                    StatusCode = HttpStatusCode.BadRequest
                };

            var userId = Guid.NewGuid();
            var personId = Guid.NewGuid();
            var password = userId.ToString().Split("-")[0];
            //var fullname = $"{register.Surname.ToUpper()} {register.Othername.ToUpper().Substring(0, 1)}. {register.Firstname.ToUpper()}";
            var dateCreated = DateTime.Now;

            user = new User
            {
                ID = userId,
                EmailAddress = register.EmailAddress,
                Password = _encryptionService.Encrypt(password),
                BVN = Utilities.GenerateNumber(10),
                DateCreated = dateCreated
            };

            var person = new Person
            {
                ID = personId,
                UserID = userId,
                Surname = register.Surname,
                Firstname = register.Firstname,
                Othername = register.Othername,
                DateOfBirth = register.DateOfBirth,
                DateCreated = dateCreated
            };

            var operatr = new Operator
            {
                PersonID = personId,
                OperatorType = register.OperatorType,
                DateCreated = dateCreated
            };

            try
            {
                await _unitOfWork.Users.Create(user);
                await _unitOfWork.People.Create(person);
                await _unitOfWork.Operators.Create(operatr);

                await _unitOfWork.Commit();

                return new ResponseMessage
                {
                    Data = await CreateUserSession(user),
                    Message = "User registered successfully!",
                    StatusCode = HttpStatusCode.OK
                };
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                return new ResponseMessage
                {
                    Data = null,
                    Message = "Error creating user." + ex.InnerException,
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }

        }

        public async Task<UserSession> Login(string emailAddress, string passowrd)
        {
            var user = await GetUserByEmail(emailAddress);
            if (user == null) return null;

            var isValid = _encryptionService.Validate(passowrd, user.Password);
            if (!isValid) return null;

            return await CreateUserSession(user);
        }

        private async Task<UserSession> CreateUserSession(User user) =>
            await _unitOfWork.Users.GetUserPersonOperatorByID(user.ID);
    }
}
