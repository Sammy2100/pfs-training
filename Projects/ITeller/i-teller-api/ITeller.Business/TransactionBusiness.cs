﻿using ITeller.Data;
using ITeller.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Business
{
    public class TransactionBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AccountBusiness _accountBusiness;

        public TransactionBusiness(IUnitOfWork unitOfWork, AccountBusiness accountBusiness)
        {
            _unitOfWork = unitOfWork;
            _accountBusiness = accountBusiness;
        }

        public async Task<List<Transaction>> GetTransactions() =>
            await _unitOfWork.Transactions.GetAll();

        public async Task<List<TransactionVM>> GetByStatus(Status status) =>
            await _unitOfWork.Transactions.GetByStatus(status);


        public async Task<Transaction> GetByID(Guid id) =>
            await _unitOfWork.Transactions.Find(id);

        public async Task<List<Transaction>> GetByOperatorID(Guid operatorId) =>
            await _unitOfWork.Transactions.GetByOperatorID(operatorId);

        public async Task Create(Transaction transaction)
        {
            await _unitOfWork.Transactions.Create(transaction);
            await _unitOfWork.Commit();
        }

        public async Task Update(Transaction transaction)
        {
            _unitOfWork.Transactions.Update(transaction);
            await _unitOfWork.Commit();
        }

        public async Task Delete(Guid id)
        {
            var entity = await GetByID(id);
            _unitOfWork.Transactions.Delete(entity);
        }

        public async Task PostCheque(PostChequeVM cheque)
        {
            var dateCreated = DateTime.Now;
            // debit account
            var account = await _accountBusiness.GetAccountByAccountNumber(cheque.AccountNumber.Trim());
            if (account == null)
            {
                account = new Account
                {
                    ID = Guid.NewGuid(),
                    BankID = cheque.BankID,
                    Name = cheque.AccountName.Trim(),
                    Number = cheque.AccountNumber.Trim(),
                    Balance = 0,
                    DateCreated = dateCreated
                };
                // create account
                await _unitOfWork.Accounts.Create(account);
            }
            else
            {
                account.Balance -= cheque.Amount;
                _unitOfWork.Accounts.Update(account);
            }


            // credit account
            var offsetAccount = await _accountBusiness.GetAccountByAccountNumber(cheque.OffsetAccountNumber.Trim());
            if (offsetAccount == null)
            {
                offsetAccount = new Account
                {
                    ID = Guid.NewGuid(),
                    BankID = cheque.OffsetBankID,
                    Name = cheque.OffsetAccountName,
                    Number = cheque.OffsetAccountNumber,
                    Balance = cheque.Amount,
                    DateCreated = dateCreated
                };
                await _unitOfWork.Accounts.Create(offsetAccount);
            }
            else
            {
                offsetAccount.Balance += cheque.Amount;
                _unitOfWork.Accounts.Update(offsetAccount);
            }

            // Transaction
            var transaction = new Transaction
            {
                ID = Guid.NewGuid(),
                AccountID = account.ID,
                OffsetAccountID = offsetAccount.ID,
                Currency = cheque.Currency,
                Amount = cheque.Amount,
                Status = Status.Pending,
                PostedBy = cheque.PostedBy,
                DatePosted = dateCreated,
                ApprovedBy = null,
                DateApproved = null,
                Reason = string.Empty
            };

            // Cheque Images
            var frontImage = await Utilities.CreateChequeModel(cheque.FrontImage, dateCreated, transaction.ID, Position.Front);
            var rearImage = await Utilities.CreateChequeModel(cheque.RearImage, dateCreated, transaction.ID, Position.Rear);

            // Initiate transaction
            await _unitOfWork.Transactions.Create(transaction);

            await _unitOfWork.Cheques.Create(frontImage);
            await _unitOfWork.Cheques.Create(rearImage);

            // commit transaction
            await _unitOfWork.Commit();
        }

        public async Task<Transaction> Approve(ApprovalVM approval) =>
            await Approval(approval, Status.Approved);

        public async Task<Transaction> Decline(ApprovalVM approval) =>
            await Approval(approval, Status.Decline);

        public async Task<Transaction> Approval(ApprovalVM approval, Status approvalStatus)
        {
            var transaction = await GetByID(approval.TransactionId);
            if (transaction == null || transaction?.Status != Status.Pending) return null;

            transaction.Status = approvalStatus;
            transaction.ApprovedBy = approval.OperatorId;
            transaction.DateApproved = DateTime.Now;
            transaction.Reason = approval.Reason;

            await Update(transaction);

            return transaction;
        }
    }
}
