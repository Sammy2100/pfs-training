﻿using ITeller.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ITeller.Business
{
    public static class Utilities
    {
        public static string GenerateNumber(int length)
        {
            Random random = new Random();
            string r = "";
            for (int i = 1; i < length + 1; i++)
                r += random.Next(0, 9).ToString();

            return r;
        }

        public static async Task<Cheque> CreateChequeModel(IFormFile uploadedFile, DateTime date, Guid transaction, Position position)
        {
            try
            {
                if (uploadedFile == null) return null;

                if (uploadedFile.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        await uploadedFile.CopyToAsync(ms);
                        return new Cheque
                        {
                            TransactionID = transaction,
                            Name = uploadedFile.FileName,
                            MimeType = uploadedFile.ContentType,
                            Image = ms.ToArray(),
                            Size = Convert.ToDecimal((uploadedFile.Length / 1024f) / 1024f),
                            Position = position,
                            DateCreated = date
                        };
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw;
            }

        }
    }
}
