﻿using ITeller.Data;
using ITeller.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Business
{
    public class ChequeBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public ChequeBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Cheque>> GetCheques() =>
            await _unitOfWork.Cheques.GetAll();

        public async Task<Cheque> GetChequeByID(Guid id) =>
            await _unitOfWork.Cheques.Find(id);

        public async Task<List<Cheque>> GetByTransactionID(Guid transactionId) =>
            await _unitOfWork.Cheques.GetByTransactionID(transactionId);

        public async Task Create(Cheque cheque)
        {
            cheque.DateCreated = DateTime.Now;
            await _unitOfWork.Cheques.Create(cheque);
            await _unitOfWork.Commit();
        }

        public async Task Update(Cheque cheque)
        {
            _unitOfWork.Cheques.Update(cheque);
            await _unitOfWork.Commit();
        }

        public async Task Delete(Guid id)
        {
            var entity = await GetChequeByID(id);
            _unitOfWork.Cheques.Delete(entity);
        }
    }
}
