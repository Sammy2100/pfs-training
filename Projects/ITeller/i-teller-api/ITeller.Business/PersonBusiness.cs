﻿using ITeller.Data;
using ITeller.Models;
using ITeller.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Business
{
    public class PersonBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly BvnService _bvnService;

        public PersonBusiness(IUnitOfWork unitOfWork, BvnService bvnService)
        {
            _unitOfWork = unitOfWork;
            _bvnService = bvnService;
        }

        public async Task<List<Person>> GetPeople() =>
            await _unitOfWork.People.GetAll();

        public async Task<List<PersonUserVM>> GetUserPeople() =>
            await _unitOfWork.People.GetUserPeople();

        public async Task<Person> GetPersonByID(Guid id) =>
            await _unitOfWork.People.Find(id);

        public async Task<Person> GetPersonByUserID(Guid id) =>
            await _unitOfWork.People.GetPersonByUserID(id);

        //public async Task<List<PersonAccountVM>> GetPeopleAccount() =>
        //    await _unitOfWork.People.GetPeopleAccount();

        public async Task Create(Person person)
        {
            //person.BVN = await _bvnService.GetBvnByPhoneNumber(person.PhoneNumber);
            await _unitOfWork.People.Create(person);
            await _unitOfWork.Commit();
        }

        public async Task Update(Person person)
        {
            _unitOfWork.People.Update(person);
            await _unitOfWork.Commit();
        }

        public async Task Delete(Guid id)
        {
            var entity = await GetPersonByID(id);
            _unitOfWork.People.Delete(entity);
        }


    }
}
