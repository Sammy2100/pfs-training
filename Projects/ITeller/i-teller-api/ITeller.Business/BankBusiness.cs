﻿using ITeller.Data;
using ITeller.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITeller.Business
{
    public class BankBusiness
    {
        private readonly IUnitOfWork _unitOfWork;

        public BankBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Bank>> GetBanks() =>
            await _unitOfWork.Banks.GetAll();

        public async Task<Bank> GetBankByID(Guid id) =>
            await _unitOfWork.Banks.Find(id);

        public async Task Create(Bank bank)
        {
            bank.DateCreated = DateTime.Now;
            await _unitOfWork.Banks.Create(bank);
            await _unitOfWork.Commit();
        }

        public async Task Update(Bank bank)
        {
            _unitOfWork.Banks.Update(bank);
            await _unitOfWork.Commit();
        }

        public async Task Delete(Guid id)
        {
            var entity = await GetBankByID(id);
            _unitOfWork.Banks.Delete(entity);
        }


    }
}
