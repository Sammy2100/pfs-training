﻿using System.Net.Http;
using System.Threading.Tasks;

namespace ITeller.Services
{
    public class BvnService
    {
        public async Task<string> GetBvnByPhoneNumber(string phoneNumber)
        {
            var bvnApi = $"https://bvn.api.nibss/GetBvnByPhoneNumber/{phoneNumber}";

            using (HttpClient httpClient = new HttpClient())
            {
                var result = await httpClient.GetStringAsync(bvnApi);
                return result;
            }
        }
    }
}
