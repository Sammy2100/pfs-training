import React, { Component } from 'react';
import CourseCard from '../Components/courses/coursecard';

class AnyName extends Component {
    
    state = { count: 1, country: "", isLoading: true}

    newFunction = () => {
        // let {count} = this.state;
        // count++;
        // this.setState({count});

        this.setState(prevState => {
            return {...this,
                count: prevState.count + 1,
            }
        })
    }

    changeCountry = () => {
        this.setState({country: "United States of America"})
    }
    componentDidMount() {
        const data = { country : "Nigeria" };

        // console.log("Mounted")
        // this.setState(prevState => {
        //     return {...this, status: !prevState.staus}
        // })
        setTimeout(() => {
            this.setState({
                country : data.country,
                isLoading: false
            });
        }, 20000);

    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     if(nextProps.something !== this.props.something) {
    //         return true
    //     }
    // }

//    /

    // componentDidUpdate(prevProps, prevState) {
    //     if(prevState.count !== this.state.count) {
    //      this.setState({color: "#fff"})   
    //     }
    // }

    render() { 
        const displayLogic = this.state.country === "" ? <div>Loading current country from server...</div> : <><div><h1 style={{color: "#fff"}}>{this.state.country}</h1></div>
        <button onClick={this.newFunction}>Change Name</button></>

        return ( 
            <div style={{color: "#fff"}}>
                {/* <p style={{color: this.state.color}}>{this.state.count}</p> */}
                { this.state.isLoading ? <div id ="div1">Loading current country from server...</div>  : <div id="div2"><h1 style={{color: "#fff"}}>{this.state.country}</h1></div> }
               
                {!this.state.isLoading && <button onClick={this.changeCountry}>Change Country</button>}
                {/* {displayLogic} */}
            </div>
            
        ); 
    }
}

 
export default AnyName;