import Animal from "./maincomponent1"

export class Cat extends Animal {
    constructor(type, legs, color) {
        super(type, legs);
        this.color = color;
    }
    animalsound() {
        return "meow"
    }
}