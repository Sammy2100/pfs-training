export default class Animal {
    constructor(typeofAnimal, legs) {
        this.type1 = typeofAnimal;
        this.legs = legs;
    }
    // constructor(typeofAnimal) {
    //     this.type1 = typeofAnimal;
    // }

    animalsound(type) {
        if(type === "Cat") {
            return "meow"
        } else if(type === "Dog") {
            return "Bark"
        } else if(type === "Chicken") {
            return "Crow"
        } else {
            return "sound"
        }
    }
}

export class Human {
    constructor(typeofHuman, race) {
        this.typeofHuman = typeofHuman;
        this.race = race;
    }

    // animalsound(type) {
    //     if(type === "Cat") {
    //         return "meow"
    //     } else if(type === "Dog") {
    //         return "Bark"
    //     } else if(type === "Chicken") {
    //         return "Crow"
    //     } else {
    //         return "sound"
    //     }
    // }
}
 export const Tiger = new Animal("Tiger", 4)

