const CourseCard = ({cardProps}) => {
 const {title, description, duration, link} = cardProps
    return (
        <div className="course-card">
            <h1>{title}</h1>
            <p>{description}</p>
            <p>{`Course Duration: ${duration} hours`}</p>
            <p>
                <a href={link}>View Complete Course</a>
            </p>
        </div>
     );
}
 
export default CourseCard;