import Footer from "./footer"
import MainContent from "./maincontent"
import Navbar from "../MainComponents1/navbarClassComponent"
import "./home.css"
import Animal, {Tiger} from "../MainComponents1/maincomponent1"
import {Cat} from "../MainComponents1/extendClass"
import AnyName from "../MainComponents1/anyname"

const Home = () => {
    
    // const myCat = new Cat("Cat", 4, "black")
    // console.log(myCat.animalsound())
    // const Cat = new Animal("Cat");
    // console.log(Cat)
    // const Dog = new Animal("Dog", 4);
    // const Chicken = new Animal("Chicken", 2);


    // Cat.color = "black"
    // console.log(Cat, Tiger)
    // console.log(Dog.animalsound("Dog"))
    // console.log(Chicken.animalsound("Chicken"))
    return ( 
        <div className="home">
            <Navbar name="Hameedah" />
            <AnyName />
            <MainContent />
            <Footer />
        </div>
     );
}
 
export default Home;