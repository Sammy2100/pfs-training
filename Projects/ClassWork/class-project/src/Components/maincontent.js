import CourseList from "./courselist";

const MainContent = () => {
    
    return ( 
        <div className="main">
            <CourseList />
        </div>
     );
}
 
export default MainContent;