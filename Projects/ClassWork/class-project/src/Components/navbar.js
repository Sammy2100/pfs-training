import Logo from "../images/32-3d-logo-design-1-500x500.jpg"

const Navbar= () => {
    return ( 
        <div className="navbar">
            <div className="logo-div">
                <img src={Logo} alt="logo" />
            </div>
            <div className="nav-menu">
                <ul>
                    <li className="active">HOME</li>
                    <li>SERVICES</li>
                    <li>WORKS</li>
                    <li>ABOUT</li>
                    <li>BLOG</li>
                    <li>CONTACT</li>
                </ul>
            </div>
            <div className="nav-social-links">
                <ul>
                    <li><i class="fa fa-behance" aria-hidden="true"></i></li>
                    <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                    <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
     );
}
 
export default Navbar